mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../amr/amr_parameters.f90 -o amr_parameters.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../amr/amr_commons.f90 -o amr_commons.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../amr/random.f90 -o random.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../pm/pm_parameters.f90 -o pm_parameters.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../patch/samgeen/P4_mergejan2014/pm_commons.f90 -o pm_commons.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../poisson/poisson_parameters.f90 -o poisson_parameters.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../poisson/poisson_commons.f90 -o poisson_commons.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../patch/samgeen/P4_mergejan2014/hydro_parameters.f90 -o hydro_parameters.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../patch/samgeen/P4_mergejan2014/hydro_commons.f90 -o hydro_commons.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../hydro/cooling_module.f90 -o cooling_module.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../amr/bisection.f90 -o bisection.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../pm/sparse_mat.f90 -o sparse_mat.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../pm/clfind_commons.f90 -o clfind_commons.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c ../pm/gadgetreadfile.f90 -o gadgetreadfile.o
../utils/scripts/cr_write_makefile.sh  Makefile
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c write_makefile.f90 -o write_makefile.o
../utils/scripts/cr_write_patch.sh ../patch/samgeen/networking:../patch/samgeen/constT:../patch/samgeen/P4_mergejan2014
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -c write_patch.f90 -o write_patch.o
mpif90 -cpp -DNVECTOR=16 -DNDIM=3 -DNPRE=8 -DNIONS=3 -DNGROUPS=3 -DNVAR=11 -DSOLVERmhd  -DIFORT -DRT -DPATCH=\'../patch/samgeen/networking:../patch/samgeen/constT:../patch/samgeen/P4_mergejan2014\' -DGITBRANCH=\'master\' -DGITHASH=\'"4b4b038d Merged rteyssie/ramses into master"\' \
-DGITREPO=\'https://samgeen:honker@bitbucket.org/samgeen/ramses_samgeen.git\' -DBUILDDATE=\'"jeu. févr. 19 13:23:40 GMT 2015"\' -c ../amr/write_gitinfo.f90 -o write_gitinfo.o
