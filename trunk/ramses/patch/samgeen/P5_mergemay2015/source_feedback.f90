subroutine make_sn
  ! Adapted from O. Iffrig
  use amr_commons
  use hydro_commons
  implicit none

  integer:: ivar
  integer:: ilevel, ind, ix, iy, iz, ngrid, iskip, idim
  integer:: i, nx_loc, igrid, ncache
  integer, dimension(1:nvector), save:: ind_grid, ind_cell
  real(dp):: dx
  real(dp):: scale, dx_min, dx_loc, vol_loc

  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::scale_msun, scale_ecgs

  real(dp), dimension(1:3):: xbound, skip_loc
  real(dp), dimension(1:twotondim, 1:3):: xc
  logical, dimension(1:nvector), save:: ok

  real(dp),dimension(1:ndim):: sn_cent
  real(dp), dimension(1:nvector, 1:ndim), save:: xx
  real(dp):: sn_r, sn_m, sn_e, sn_vol, sn_d, sn_ed, dx_sel
  real(dp):: rr, pi
  logical:: sel = .false.
  !logical, save:: first = .true.

  if(.not. hydro)return
  if(ndim .ne. 3)return

  if(verbose)write(*,*)'Entering make_sn'

  pi = acos(-1.0)

  ! Mesh spacing in that level
  xbound(1:3) = (/ dble(nx), dble(ny), dble(nz) /)
  nx_loc = icoarse_max - icoarse_min + 1
  skip_loc = (/ 0.0d0, 0.0d0, 0.0d0 /)
  skip_loc(1) = dble(icoarse_min)
  skip_loc(2) = dble(jcoarse_min)
  skip_loc(3) = dble(kcoarse_min)
  scale = boxlen / dble(nx_loc)
  dx_min = scale * 0.5d0**nlevelmax

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_msun = scale_d * scale_l**ndim / m_sun
  scale_ecgs = scale_d * scale_v**2

  ! Hard-code sn properties to centre of box
  sn_r = 3.*(0.5**levelmin)*scale
  sn_m = 80d0 / scale_msun ! I dunno?
  sn_e = 1e51 / scale_ecgs
  sn_cent(1)= 0.5*boxlen
  sn_cent(2)= 0.5*boxlen
  sn_cent(3)= 0.5*boxlen

  if(sn_r /= 0.0) then
     sn_vol = 4. / 3. * pi * sn_r**3
     sn_d = sn_m / sn_vol
     sn_ed = sn_e / sn_vol
  end if

  if(myid .eq. 1) then
     write(*,*) 'Supernova blast! Wow!'
     write(*,*) 'x_sn, y_sn, z_sn, ',sn_cent(1),sn_cent(2),sn_cent(3)
  endif

  ! Loop over levels
  do ilevel = levelmin, nlevelmax
    ! Computing local volume (important for averaging hydro quantities)
    dx = 0.5d0**ilevel
    dx_loc = dx * scale
    vol_loc = dx_loc**ndim
    !if(.not. sel) then
      ! dx_sel will contain the size of the biggest leaf cell around the center
      !dx_sel = dx_loc
      !sn_vol = vol_loc
    !end if

    ! Cell center position relative to grid center position
    do ind=1,twotondim
      iz = (ind - 1) / 4
      iy = (ind - 1 - 4 * iz) / 2
      ix = (ind - 1 - 2 * iy - 4 * iz)
      xc(ind,1) = (dble(ix) - 0.5d0) * dx
      xc(ind,2) = (dble(iy) - 0.5d0) * dx
      xc(ind,3) = (dble(iz) - 0.5d0) * dx
    end do

    ! Loop over grids
    ncache=active(ilevel)%ngrid
    do igrid = 1, ncache, nvector
      ngrid = min(nvector, ncache - igrid + 1)
      do i = 1, ngrid
        ind_grid(i) = active(ilevel)%igrid(igrid + i - 1)
      end do

      ! Loop over cells
      do ind = 1, twotondim
        ! Gather cell indices
        iskip = ncoarse + (ind - 1) * ngridmax
        do i = 1, ngrid
          ind_cell(i) = iskip + ind_grid(i)
        end do

        ! Gather cell center positions
        do i = 1, ngrid
          xx(i, :) = xg(ind_grid(i), :) + xc(ind, :)
        end do
        ! Rescale position from coarse grid units to code units
        do i = 1, ngrid
           xx(i, :) = (xx(i, :) - skip_loc(:)) * scale
        end do

        ! Flag leaf cells
        do i = 1, ngrid
          ok(i) = (son(ind_cell(i)) == 0)
        end do

        do i = 1, ngrid
          if(ok(i)) then
            if(sn_r == 0.0) then
              sn_d = sn_m / vol_loc ! XXX
              sn_ed = sn_e / vol_loc ! XXX
              rr = 1.0
              do idim = 1, 3
                !rr = rr * max(1.0 - abs(xx(i, idim) - sn_center(sn_i, idim)) / dx_sel, 0.0)
                rr = rr * max(1.0 - abs(xx(i, idim) - sn_cent(idim)) / dx_loc, 0.0)
              end do
              !if(rr > 0.0) then
                !if(.not. sel) then
                  !! We found a leaf cell near the supernova center
                  !sel = .true.
                  !sn_d = sn_m / sn_vol
                  !sn_ed = sn_e / sn_vol
                !end if
                uold(ind_cell(i), 1) = uold(ind_cell(i), 1) + sn_d * rr
                uold(ind_cell(i), 5) = uold(ind_cell(i), 5) + sn_ed * rr
              !end if
            else
              rr = sum(((xx(i, :) - sn_cent(:)) / sn_r)**2)

              if(rr < 1.) then
                uold(ind_cell(i), 1) = uold(ind_cell(i), 1) + sn_d
                uold(ind_cell(i), 5) = uold(ind_cell(i), 5) + sn_ed
              endif
            endif
          endif
        end do
      end do
      ! End loop over cells
    end do
    ! End loop over grids
  end do
  ! End loop over levels

  ! Update hydro quantities for split cells
  do ilevel = nlevelmax, levelmin, -1
    call upload_fine(ilevel)
    do ivar = 1, nvar
      call make_virtual_fine_dp(uold(1, ivar), ilevel)
    enddo
  enddo
end subroutine make_sn

!################################################################
!################################################################
!################################################################
!################################################################

SUBROUTINE source_refine(xx,ok,ncell,ilevel)

! This routine flags cells immediately around SN sources to the finest
! level of refinement. The criteria for refinement at a point are:
! a) The point is less than one ilevel cell width from an SN source.
! b) The point is within SN_r_wind finest level cell widths from
!    the SN source.
!-------------------------------------------------------------------------
  use amr_commons
  use pm_commons
  use hydro_commons
  use poisson_commons
  implicit none
  integer::ncell,ilevel,i,k,nx_loc,rcellsn
  real(dp),dimension(1:nvector,1:ndim)::xx
  logical ,dimension(1:nvector)::ok
  real(dp)::dx_loc,rvec(ndim),w,rmag,rSN
!-------------------------------------------------------------------------
  nx_loc=(icoarse_max-icoarse_min+1)
  dx_loc = boxlen*0.5D0**ilevel/dble(nx_loc)
  rcellsn = 10 ! HARD-CODE TO N CELLS
  rSN = rcellsn*boxlen*0.5D0**nlevelmax 
  ! Loop over regions
  !do k=1,n_SN_source
     do i=1,ncell
        rvec(:)=xx(i,:)-SN_pos(k,:)
        rmag=sqrt(sum(rvec**2))
        if(rmag .le. 2*rSN+dx_loc) then
           ok(i)=.true.
        endif
     end do
  !end do
  
END SUBROUTINE source_refine


!################################################################
!################################################################
!################################################################
!################################################################
!*************************************************************************
SUBROUTINE read_source_params(nml_ok)

! Read ideal_SN_params namelist
!-------------------------------------------------------------------------
  use amr_commons
  !use sed_module
  implicit none
  logical::nml_ok
  integer::i
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::fmassout
  character(len=40)::logfilename
!-------------------------------------------------------------------------
  namelist/IDEAL_SN_PARAMS/ n_SN_source, SN_posx, SN_posy, SN_posz       &
       & ,SN_stellar_mass_solar, SN_wind, SN_metallicity, SN_tabledir    &
       & ,SN_r_wind, source_odd, w_speed_cgs, w_mdot_cgs, w_TK           &
       & ,wind_partition, wind_thermal_pulse, wind_noise_max             &
       & ,SN_self_gravity
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  rewind(1)
  read(1,NML=IDEAL_SN_PARAMS,END=101)
  do i=1,n_SN_source
     SN_pos(i,1)=SN_posx(i)
     if(ndim .gt. 1) SN_pos(i,2)=SN_posy(i)
     if(ndim .gt. 2) SN_pos(i,3)=SN_posz(i)
  end do
  dx_levelmax=boxlen*0.5D0**nlevelmax
  ! Make the wind sphere to deposit the SN winds
  if(SN_wind(1)) call prepare_Wind_Weights(1000000)
101 continue
  if(myid==1) write(*,*) 'Initializing SED table for stellar source'
  ! Set wind energy partition (wind_thermal pulse forces wind_partition to 1.)
  if (wind_thermal_pulse) wind_partition = 1.
  if(myid==1) then 
     if (wind_thermal_pulse) &
          write(*,*) 'Modelling stellar wind as a thermal pulse' 
     if ((wind_partition .ge. 0.) .and. (wind_partition .le. 1.)) &
          write(*,*) 'Using fixed stellar wind energy partition of ', &
          wind_partition
    endif
  !if(rt) call init_SED_table()
  !call update_SN_PacProps()
  if(SN_tabledir .ne. '') is_feedbk_file=.true. 
  !call setup_single_star(SN_tabledir)
  !do i=1,n_SN_source
  !   call star_snparams(SN_stellar_mass_solar(i), SN_metallicity(i)      &
  !        ,SN_mremnant_solar(i), SN_time_myr(i),  SN_E_cgs(i),SN_yield(i))
  !enddo
  !SN_E_cgs(1:n_SN_source)=SN_E_cgs(1:n_SN_source)*1.d51
  SN_E_cgs(1:n_SN_source)=1.d51
  SN_time_myr(1:n_SN_source) = 1d0
  SN_time_uu(1:n_SN_source) = &
       SN_time_myr(1:n_SN_source)*3.1556926d13/scale_t
  ! TODO: ADD AS PARAMETER

  ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! HACK HACK HACK!!! IF NO WIND, JUMP STRAIGHT TO SN
  ! THIS PREVENTS ERRORS CREEPING IN DUE TO COOLING, GRAVITY, ETC
  !if (.not. SN_wind(1) .and. n_SN_source .eq. 1) then
  !   write(*,*) "JUMPING TO SN AT TIME",SN_time_myr(1)
  !   t = SN_time_uu(1) ! ASSUMES ONE SOURCE ONLY!!!!!!!
  !endif
  ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  if(myid==1) then
     write(*,*),'Ideal supernova properties:-----------------------------'
     write(*,*), "n_SN_source = ",n_SN_source
     write(*,'(a, 20(1pe16.6))')                                         &
          '  Stellar mass [Solar] = ',SN_stellar_mass_solar(1:n_SN_source) 
     write(*,'(a, 20(1pe16.6))')                                         &
          '  SN time [Myr]        = ',SN_time_myr(1:n_SN_source) 
     write(*,'(a, 20(1pe16.6))')                                         &
          '  SN E [erg]           = ',SN_e_cgs(1:n_SN_source) 
     write(*,'(a, 20(1pe16.6))')                                         &
          '  Remnant mass [Solar] = ',SN_mremnant_solar(1:n_SN_source) 
     write(*,'(a, 20(1pe16.6))')                                         &
          '  SN Z (absolute)     = ',SN_metallicity(1:n_SN_source)
     if(.not. is_feedbk_file) then
        write(*,'(a, 1pe16.6)') '  Wind speed [cm/s] = ',w_speed_cgs
        write(*,'(a, 1pe16.6)') '  Mass loss [g s]   = ',w_mdot_cgs
        write(*,'(a, 1pe16.6)') '  Wind T [K]        = ',w_TK
     endif
     write(*,*),'--------------------------------------------------------'
  endif
  ! Open SNe log file (too slow!)
  !logfilename='logSNe.dat'
  !open(unit=137,file=logfilename,status='replace')
  !write(unit=137,fmt=*) "Mass momentum kinetic_energy thermal_energy speed temperature"

END SUBROUTINE read_source_params


!################################################################
!################################################################
!################################################################
!################################################################
