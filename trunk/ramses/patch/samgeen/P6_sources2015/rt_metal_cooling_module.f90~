MODULE rt_metal_cooling_module

! Returns metal cooling values
! Sam Geen, September 2014

! Use lookup table module for speed, mass-loss arrays
use amr_parameters
use hydro_parameters
use lookup_table_module

implicit none

public

! Include ISM heating model?
logical :: ISM_heating_switch

! Tables for metal cooling with and without flux
type(lookup_table) :: metal_table
type(lookup_table) :: metal_noflux_table

CONTAINS

!************************************************************************
! Sets up the tables, and then clears them (e.g. on program exit)
SUBROUTINE setup_metal_cooling(dir)
  use amr_commons
  character(len=128)              ::dir,filename
  ISM_heating_switch=.true.
  filename = 'cooltable.dat'
  call setup_table(metal_table, filename)
  filename = 'cooltablenoflux.dat'
  call setup_table(metal_noflux_table, filename)
  if (myid .eq. 1) write(*,*) "Set up metal cooling!"
END SUBROUTINE setup_metal_cooling

SUBROUTINE cleanup_metal_cooling
  call clear_table(metal_table)
  call clear_table(metal_noflux_table)
END SUBROUTINE cleanup_metal_cooling

!************************************************************************
! Calculates metal cooling rates using tables that include photon flux
SUBROUTINE rt_metal_cool(Tin,Nin,Fin,xin,mu,ne,metal_tot,metal_prime)
  ! Taken from the equilibrium cooling_module of RAMSES
  ! Compute cooling enhancement due to metals
  ! Tin          => Temperature in Kelvin, divided by mu
  ! Nin          => Hydrogen number density (H/cc)
  ! Fin          => Photon flux in lowest energy group (erg /cm^2/s)
  !              => Can be calculated as Np_i * c_red * e_group
  ! xin          => Hydrogen ionisation fraction (0->1)
  ! mu           => Average mass per particle in terms of mH
  ! ne           => Electron number density
  ! metal_tot   <=  Metal cooling contribution to de/dt / (nH*ne) [erg s-1 cm-3]
  ! metal_prime <=  d(metal_tot)/dT2 / (nH*ne) [erg s-1 cm-3 K-1]
  real(dp),intent(in)::Tin,Nin,Fin,mu,ne,xin
  real(dp)::T1,T2,nH,flux,cool1,cool2,eps
  real(dp),intent(out)::metal_tot,metal_prime

  ! Set a reference temperature to calculate gradient wrt T
  eps = 1e-5 ! A small value
  T1 = Tin*mu
  T2 = Tin*(1+eps)*mu
  
  ! Call a function mixing the two cooling functions
  call rt_metal_cool_mashup(T1,Nin,Fin,xin,ne,cool1)
  call rt_metal_cool_mashup(T2,Nin,Fin,xin,ne,cool2)
  
  ! Don't cool below 10K to prevent bound errors, but allow heating
  if ((Tin*mu .gt. 10d0) .or. (cool1 .lt. 0d0)) then
     ! Calculate gradient and output
     metal_tot = cool1
     ! T2 = T*(1+eps), so T2-T == eps*T
     metal_prime = (cool2 - cool1) / (Tin * mu * eps)
     ! NOTE !!!! NEED TO MULTIPLY BY nH*ne AFTER THIS IS OVER!!!!
     ! EXCLAMATION MARK EXCLAMATION MARK
  else
     ! Prevent runaway cooling below 10K
     metal_tot = 0d0
     metal_prime = 0d0
  endif

END SUBROUTINE rt_metal_cool

!************************************************************************
! Mixes Patrick Hennebelle's cooling function with Alex Riching's table
! Uses a sigmoid function centred at x=0.1 with a +/-0.05 spread to switch
SUBROUTINE rt_metal_cool_mashup(T,N,F,x,ne,cool)

  ! Taken from the equilibrium cooling_module of RAMSES
  ! Compute cooling enhancement due to metals
  ! T            => Temperature in Kelvin *with mu included*
  ! N            => Hydrogen number density (H/cc)
  ! F            => Photon flux in lowest energy group (erg /cm^2/s)
  !              => Can be calculated as Np_i * c_red * e_group
  ! x            => Hydrogen ionisation fraction (0->1)
  ! ne           => Electron number density
  ! cool         <=  Metal cooling [erg s-1 cm-3]

  real(dp),intent(in)::T,N,F,x,ne
  real(dp)::logT,logN,logF,sig,coolar,coolph,dummy,drefdt
  real(dp),intent(out)::cool
  ! HII region observed abundances and Cloudy should be consistent
  ! So don't use RAMSES-fiducial value for Z as it's only used here, where
  ! it's wrong
  real(dp),parameter::Zfact = 1.0! 0.02/0.0129 -> Want to model 'true' Zsolar
  ! ISM UV background flux calculated as ~10^-8.5 erg cm^-2 s^-1
  ! Based on heating rate 2e-26 erg/s / cross section 3e-18 cm^2
  real(dp),parameter::logbkgflux = -8.5

  logT = log10(T)
  logN = log10(N)
  logF = log10(F)

  ! Set a UV background based on 2e-26 erg/s at the lowest group energy
  logF = MAX(logF,logbkgflux)


  cool = 0.0
  if (logT .ge. 6.0) then
     ! If T > 1e6K, use the zero flux "everything is collisionally 
     !    ionised" table
     call find_value2(metal_noflux_table, logN, logT, cool)
     cool = cool*ne*N*Zfact
  !else if (logT .ge. 4.0)
  !   ! If the temperature is over 1e4K the PH function doesn't work 
  !   !    so use the table, but with the flux
  !   call find_value3(metal_table, logF, logN, logT, cool)
  !   cool = cool*ne*N*Zfact
  else
     ! Make mashup routine where 
     call find_value3(metal_table, logF, logN, logT, coolar)
     coolar = coolar*ne*N*Zfact
     !call calc_cooling_rate(T,n,x,coolph)
     if (T < 10035.d0) then
        call chaud_froid_1(T,N,x,cool,dRefdT)
     else
        call chaud_froid_2(T,N,cool,dRefdT)
     end if
     cool = coolar * x - coolph * (1.0-x)
  endif

END SUBROUTINE rt_metal_cool_mashup


END MODULE
