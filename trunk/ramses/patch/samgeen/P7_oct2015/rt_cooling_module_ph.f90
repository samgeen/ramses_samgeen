!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!=======================================================================
subroutine  rt_calc_rates_ph(Tin,Nin,mu,ref,drefdT,Xin)
! Taken from the equilibrium cooling_module of RAMSES
! Compute cooling enhancement due to metals
! Tin          => Temperature in Kelvin, divided by mu
! Nin          => Hydrogen number density (H/cc)
! mu           => Average mass per particle in terms of mH
! ref          <= Metal cooling contribution to de/dt [erg s-1 cm-3]
! drefdT       <= d(metal_tot)/dTT [erg s-1 cm-3 K-1]
! Xin          => 
!=======================================================================
    use amr_parameters
    use hydro_commons

    implicit none

    !real(dp) :: mm,uma, kb, alpha,mu,kb_mm
    real(dp) :: mu
    real(dp) :: NN,TT,ref,dRefdT, XX, Nin, Tin, Xin

    !
    ! Cette routine fonctionne en cgs
    ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    !  kb  =  1.38062d-16   ! erg/K
    !  uma =  1.660531e-24  ! grams
    !  mu  =  1.4
    !  mm = mu*uma
    !  kb_mm = kb / mm
    !  TT = TT  / kb  !/ kb_mm

    ! Sanitise the inputs
    ref = 0d0
    drefdT = 0d0
    TT = Tin
    NN = Nin
    XX = Xin

    ! Set a minimum temperature
    if( TT .le. 0.) then
        TT = 50 / mu
        return
    endif

    ! TT inputted is in K/mu, so multiply by mu given in rt_cmp_metals
    TT     = TT * mu

    if (NN .le. smallr) then
        if( NN .le. 0)  &
             write(*,*) '!! Negative densities in rt_calc_rates_ph',NN
        NN = smallr
    endif

    ! Conversion factor from erg/cm^3/s to K, for reference
    !     alpha = NN*kb_mm/(gamma-1.)
    !alpha = NN*kb/(gamma-1.)

    ! Fix negative temperatures if something has gone badly wrong
    if (TT .lt.0) then
       write(*,*) 'prob Temp',TT, NN
       NN = max(NN,smallr)
       TT = min(4000./NN,8000.)  !2.*4000. / NN
    endif

    !
    if (TT < 10035.d0) then
       call chaud_froid_1(TT,NN,XX,ref,dRefdT)
    else
       call chaud_froid_2(TT,NN,ref,dRefdT)
    end if
    ! Needs to be flipped in sign to interface with Joki's module
    ref = -ref
    dRefdT = -dRefdT

    return
end subroutine rt_calc_rates_ph

!=======================================================================
subroutine  rt_calc_refonly(Tin,Nin,mu,ref,drefdT,Xin)
! Taken from the equilibrium cooling_module of RAMSES
! Compute cooling enhancement due to metals
! Tin          => Temperature in Kelvin, divided by mu
! Nin          => Hydrogen number density (H/cc)
! mu           => Average mass per particle in terms of mH
! ref          <= Metal cooling contribution to de/dt [erg s-1 cm-3]
! drefdT       <= d(metal_tot)/dTT [erg s-1 cm-3 K-1]
! Xin          => 
!=======================================================================
    use amr_parameters
    use hydro_commons

    implicit none

    !real(dp) :: mm,uma, kb, alpha,mu,kb_mm
    real(dp) :: mu
    real(dp) :: NN,TT,ref,dRefdT, XX, Nin, Tin, Xin

    !
    ! Cette routine fonctionne en cgs
    ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    !  kb  =  1.38062d-16   ! erg/K
    !  uma =  1.660531e-24  ! grams
    !  mu  =  1.4
    !  mm = mu*uma
    !  kb_mm = kb / mm
    !  TT = TT  / kb  !/ kb_mm

    ! Sanitise the inputs
    ref = 0d0
    drefdT = 0d0
    TT = Tin
    NN = Nin
    XX = Xin

    ! Set a minimum temperature
    if( TT .le. 0.) then
        TT = 50 / mu
        return
    endif

    ! TT inputted is in K/mu, so multiply by mu given in rt_cmp_metals
    TT     = TT * mu

    if (NN .le. smallr) then
        if( NN .le. 0)  &
             write(*,*) '!! Negative densities in rt_calc_rates_ph',NN
        NN = smallr
    endif

    ! Conversion factor from erg/cm^3/s to K, for reference
    !     alpha = NN*kb_mm/(gamma-1.)
    !alpha = NN*kb/(gamma-1.)

    ! Fix negative temperatures if something has gone badly wrong
    if (TT .lt.0) then
       write(*,*) 'prob Temp',TT, NN
       NN = max(NN,smallr)
       TT = min(4000./NN,8000.)  !2.*4000. / NN
    endif

    !
    if (TT < 10035.d0) then
       call chaud_froid_1(TT,NN,XX,ref,dRefdT)
    else
       call chaud_froid_2(TT,NN,ref,dRefdT)
    end if
    ! Needs to be flipped in sign to interface with Joki's module
    ref = -ref
    dRefdT = -dRefdT

    return
end subroutine rt_calc_refonly
