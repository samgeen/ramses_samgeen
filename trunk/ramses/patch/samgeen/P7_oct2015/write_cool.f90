SUBROUTINE write_cool
  !-------------------------------------------------------------------!
  ! Write the cooling table being used out to file                    !
  ! Sam Geen, October 2015                                            !
  !-------------------------------------------------------------------!

  ! Function called for reference
  ! rt_metal_cool(dT2,nH(icell),flux,dXion(1),mu,ne,metal_tot,metal_prime)

  ! This is hard-coded for STG's metal cooling function
  use rt_metal_cooling_module

  implicit none

  integer :: iD,iT
  integer,parameter :: nD=4
  integer,parameter :: nT=200 
  real(kind=8) :: ne, nH, T, x, mu
  real(kind=8) :: metal_tot,metal_prime
  real(kind=8), dimension(nD) :: Ds
  real(kind=8), dimension(nT) :: Ts
  real(kind=8), dimension(nD,nT) :: out_rate
  real(kind=8), dimension(nD,nT) :: out_rate_noflux


  write(*,*) "Writing cooling table..."


  ! Set temperature, density arrays
  !Ds = 0d0
  Ds(1) = -2d0
  Ds(2) = 0d0
  Ds(3) = 2d0
  Ds(4) = 4d0
  !do iD=1,nD
  !   ! -2 to 4 in log space
  !   Ds(iD) = -2d0 + real(iD-1,kind=8)/real(nD,kind=8)*6d0
  !end do
  Ts = 0d0
  do iT=1,nT
     ! 0 to 7 in log space
     Ts(iT) = real(iT-1,kind=8)/real(nT,kind=8)*7d0
  end do

  ! Make tables
  do iD=1,nD
     do iT=1,nT
        ! Make no flux tables (log flux = -11)
        T = 10d0**Ts(iT)
        ne = 2.4E-3*((T/100.)**0.25)/0.5 
        x = max(ne / nH,0.1)
        mu = 1./(0.76)!*(1.+x))
        ! HACK - above 1e4 K, assume collisionally ionised ???
        ! if (T.gt.1e4) x = 1d0
        nH = 10d0**Ds(iD)
        ne = nH*x
        call rt_metal_cool(10d0**Ts(iT), &
             nH, &
             1d-11, &
             x,mu, &
             ne, &
             metal_tot,metal_prime)
        out_rate_noflux(iD,iT) = metal_tot / (nH*ne)
        ! Make tables with flux (log flux = 1, why not)
        T = 10d0**Ts(iT)
        ne = 2.4E-3*((T/100.)**0.25)/0.5 
        nH = 10d0**Ds(iD)
        x = 1d0 ! HACK to ignore 0.1 limit!
        ne = nH*x
        mu = 1./(0.76)!*(1.+x))
        call rt_metal_cool(T, &
             nH, &
             1d-11, &
             x,mu, &
             ne, &
             metal_tot,metal_prime)
        out_rate(iD,iT) = metal_tot / (nH*ne)
     end do
  end do

  ! Write no flux file
  open(unit=8075,file='./cool_noflux.out',form='unformatted')
  write(8075) nD,nT
  write(8075) Ds
  write(8075) Ts
  write(8075) out_rate_noflux
  close(8075)

  ! Write flux file
  open(unit=8075,file='./cool_flux.out',form='unformatted')
  write(8075) nD,nT
  write(8075) Ds
  write(8075) Ts
  write(8075) out_rate
  close(8075)

  write(*,*) "Cooling table written, stopping..."

  call clean_stop

END SUBROUTINE write_cool
