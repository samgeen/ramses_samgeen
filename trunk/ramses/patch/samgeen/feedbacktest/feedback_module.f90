module feedback_module
  use amr_parameters,only:dp,ndim
  implicit none

  public

  ! Use the supernova module?
  logical::SN_on = .false.

  ! Number of supernovae (max limit and number active in namelist)
  integer,parameter::NSNMAX=1000
  integer::SN_nsource

  ! Supernova times and whether they're done or not
  real(dp),dimension(1:NSNMAX)::SN_time = 1d10
  logical,dimension(1:NSNMAX)::SN_done = .false.
  real(dp),dimension(1:NSNMAX)::SN_pos_x = 0.5d0
  real(dp),dimension(1:NSNMAX)::SN_pos_y = 0.5d0
  real(dp),dimension(1:NSNMAX)::SN_pos_z = 0.5d0
  real(dp),dimension(1:NSNMAX)::SN_mejecta = 1d0
  real(dp),dimension(1:NSNMAX)::SN_energy = 1d51
  logical,dimension(1:NSNMAX)::SN_thermal = .false.
  real(dp),dimension(1:NSNMAX)::SN_radius = 12d0

  ! Jet stuff
  real(kind=8),allocatable,dimension(:)::vol_gas_jet,vol_gas_jet_all


CONTAINS

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

subroutine feedback_sn(ilevel)
  use amr_commons
  use hydro_parameters
  implicit none
  integer::ilevel,isn
!---------------------
  ! Supernova explosions
  !---------------------
  if(SN_on .and. ilevel == levelmin) then
     do isn=1,SN_nsource
        if(t >= sn_time(isn) .and. .not. sn_done(isn)) then
           if(myid == 1) write (*,*) 'Supernova ',isn,' @ t = ', t, &
                & 'sn_time =', sn_time(isn)
           call make_sn_blast(isn)
           sn_done(isn) = .true.
        endif
     end do
  endif
end subroutine feedback_sn

subroutine make_sn_blast(isn)
  ! Adapted from O. Iffrig
  use amr_commons
  use hydro_commons
  use rt_parameters,only:m_sun
  implicit none

  integer, intent(in) :: isn

  integer:: ivar
  integer:: ilevel, ind, ix, iy, iz, ngrid, iskip, idim
  integer:: i, nx_loc, igrid, ncache
  integer, dimension(1:nvector), save:: ind_grid, ind_cell
  real(dp):: dx
  real(dp):: scale, dx_min, dx_loc, vol_loc

  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::scale_msun, scale_ecgs

  real(dp), dimension(1:3):: xbound, skip_loc
  real(dp), dimension(1:twotondim, 1:3):: xc
  logical, dimension(1:nvector), save:: ok

  real(dp),dimension(1:ndim):: sn_cent
  real(dp), dimension(1:nvector, 1:ndim), save:: xx
  real(dp):: sn_r, sn_m, sn_e, sn_vol, sn_d, sn_ed, dx_sel, sn_p, sn_v
  real(dp):: rr, pi
  real(dp), dimension(1:ndim)::rvec
  logical:: sel = .false.
  !logical, save:: first = .true.
  !logical,dimension(1:n_source), save:: sn_done = .false.

  if(.not. hydro)return
  if(ndim .ne. 3)return

  if(verbose)write(*,*)'Entering make_sn'

  pi = acos(-1.0)

  ! Mesh spacing in that level
  xbound(1:3) = (/ dble(nx), dble(ny), dble(nz) /)
  nx_loc = icoarse_max - icoarse_min + 1
  skip_loc = (/ 0.0d0, 0.0d0, 0.0d0 /)
  skip_loc(1) = dble(icoarse_min)
  skip_loc(2) = dble(jcoarse_min)
  skip_loc(3) = dble(kcoarse_min)
  scale = boxlen / dble(nx_loc)
  dx_min = scale * 0.5d0**nlevelmax

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_msun = scale_d * scale_l**ndim / m_sun
  scale_ecgs = scale_d * scale_v**2 * scale_l**ndim

  ! Hard-code sn properties to centre of box
  sn_r = sn_radius(isn)*(0.5**nlevelmax)*scale

  !sn_m = 80d0 / scale_msun ! I dunno?
  sn_m = SN_mejecta(isn) / scale_msun ! Put in 10 solar masses
  sn_e = SN_energy(isn) / scale_ecgs
  sn_v = 0d0
  sn_v = sqrt(2.0*(sn_e*scale_ecgs)/(sn_m*scale_msun*m_sun))
  sn_v = sn_v / scale_v
  sn_cent(1)= SN_pos_x(isn)*boxlen
  sn_cent(2)= SN_pos_y(isn)*boxlen
  sn_cent(3)= SN_pos_z(isn)*boxlen
     
  ! HACK !!! - KINETIC BLAST ONLY WORKS FOR sn_r > 0.0 !!!
  if(sn_r /= 0.0) then
     sn_vol = 4. / 3. * pi * sn_r**3
     sn_d = sn_m / sn_vol
     sn_ed = sn_e / sn_vol
     sn_p = sn_d*sn_v ! uniform momentum of blast ejecta
  end if
     
  if(myid .eq. 1) then
     write(*,*) 'Supernova blast! Wow!'
     write(*,*) 'x_sn, y_sn, z_sn, ',sn_cent(1),sn_cent(2),sn_cent(3)
  endif

  ! Loop over levels
  do ilevel = levelmin, nlevelmax
    ! Computing local volume (important for averaging hydro quantities)
    dx = 0.5d0**ilevel
    dx_loc = dx * scale
    vol_loc = dx_loc**ndim
    !if(.not. sel) then
      ! dx_sel will contain the size of the biggest leaf cell around the center
      !dx_sel = dx_loc
      !sn_vol = vol_loc
    !end if

    ! Cell center position relative to grid center position
    do ind=1,twotondim
      iz = (ind - 1) / 4
      iy = (ind - 1 - 4 * iz) / 2
      ix = (ind - 1 - 2 * iy - 4 * iz)
      xc(ind,1) = (dble(ix) - 0.5d0) * dx
      xc(ind,2) = (dble(iy) - 0.5d0) * dx
      xc(ind,3) = (dble(iz) - 0.5d0) * dx
    end do

    ! Loop over grids
    ncache=active(ilevel)%ngrid
    do igrid = 1, ncache, nvector
      ngrid = min(nvector, ncache - igrid + 1)
      do i = 1, ngrid
        ind_grid(i) = active(ilevel)%igrid(igrid + i - 1)
      end do

      ! Loop over cells
      do ind = 1, twotondim
        ! Gather cell indices
        iskip = ncoarse + (ind - 1) * ngridmax
        do i = 1, ngrid
          ind_cell(i) = iskip + ind_grid(i)
        end do

        ! Gather cell center positions
        do i = 1, ngrid
          xx(i, :) = xg(ind_grid(i), :) + xc(ind, :)
        end do
        ! Rescale position from coarse grid units to code units
        do i = 1, ngrid
           xx(i, :) = (xx(i, :) - skip_loc(:)) * scale
        end do

        ! Flag leaf cells
        do i = 1, ngrid
          ok(i) = (son(ind_cell(i)) == 0)
        end do

        do i = 1, ngrid
          if(ok(i)) then
            if(sn_r == 0.0) then
              sn_d = sn_m / vol_loc ! XXX
              sn_ed = sn_e / vol_loc ! XXX
              rr = 1.0
              do idim = 1, 3
                !rr = rr * max(1.0 - abs(xx(i, idim) - sn_center(sn_i, idim)) / dx_sel, 0.0)
                rr = rr * max(1.0 - abs(xx(i, idim) - sn_cent(idim)) / dx_loc, 0.0)
              end do
              !if(rr > 0.0) then
                !if(.not. sel) then
                  !! We found a leaf cell near the supernova center
                  !sel = .true.
                  !sn_d = sn_m / sn_vol
                  !sn_ed = sn_e / sn_vol
                !end if
                uold(ind_cell(i), 1) = uold(ind_cell(i), 1) + sn_d * rr
                uold(ind_cell(i), 5) = uold(ind_cell(i), 5) + sn_ed * rr
              !end if
            else
               ! Get direction to point the explosion in
               rvec = (xx(i, :) - sn_cent(:)) / sn_r
               rr = sqrt(sum(rvec**2))
               rvec = rvec/rr
   
               if(rr < 1.) then
                  uold(ind_cell(i), 1) = uold(ind_cell(i), 1) + sn_d
                  ! If not entirely thermal injection, add some velocity
                  if (.not.SN_thermal(isn)) then
                     uold(ind_cell(i),2:4) = uold(ind_cell(i),2:4) + &
                          & sn_p * rvec
                  end if
                  uold(ind_cell(i), 5) = uold(ind_cell(i), 5) + sn_ed
               endif
            endif
          endif
        end do
      end do
      ! End loop over cells
    end do
    ! End loop over grids
  end do
  ! End loop over levels

  ! Update hydro quantities for split cells
  do ilevel = nlevelmax, levelmin, -1
    call upload_fine(ilevel)
    do ivar = 1, nvar
      call make_virtual_fine_dp(uold(1, ivar), ilevel)
    enddo
  enddo
end subroutine make_sn_blast

!################################################################
!################################################################
!################################################################
!################################################################

SUBROUTINE feedback_refine(xx,ok,ncell,ilevel)

! This routine flags cells immediately around SN sources to the finest
! level of refinement. The criteria for refinement at a point are:
! a) The point is less than one ilevel cell width from an SN source.
! b) The point is within SN_r_wind finest level cell widths from
!    the SN source.
!-------------------------------------------------------------------------
  use amr_commons
  use pm_commons
  use hydro_commons
  use poisson_commons
  implicit none
  integer::ncell,ilevel,i,k,nx_loc,rcellfb,isn
  real(dp),dimension(1:nvector,1:ndim)::xx
  logical ,dimension(1:nvector)::ok
  real(dp)::dx_loc,rvec(ndim),w,rmag,rFB
!-------------------------------------------------------------------------
  nx_loc=(icoarse_max-icoarse_min+1)
  dx_loc = boxlen*0.5D0**ilevel/dble(nx_loc)
  rcellfb = 10 ! HARD-CODE TO N CELLS
  rFB = rcellfb*boxlen*0.5D0**nlevelmax
  ! Loop over regions
  do i=1,ncell
     do isn=1,SN_nsource
        rvec(1)=xx(i,1)-SN_pos_x(isn)*boxlen
        rvec(2)=xx(i,2)-SN_pos_y(isn)*boxlen
        rvec(3)=xx(i,3)-SN_pos_z(isn)*boxlen
        rmag=sqrt(sum(rvec**2))
        if(rmag .le. 2*rFB+dx_loc) then
           ok(i)=.true.
        endif
     end do
  end do
  
END SUBROUTINE feedback_refine


!################################################################
!################################################################
!################################################################
!################################################################
!*************************************************************************
SUBROUTINE read_feedback_params(nml_ok)

! Read FEEDBACK_PARAMS namelist
!-------------------------------------------------------------------------
  use amr_commons
  implicit none
  logical::nml_ok
!-------------------------------------------------------------------------
  namelist/FEEDBACK_PARAMS/ SN_nsource, SN_on, SN_time, &
       & SN_pos_x, SN_pos_y, SN_pos_z, &
       & SN_mejecta, SN_energy, SN_thermal, SN_radius
  rewind(1)
  read(1,NML=FEEDBACK_PARAMS,END=101)
101 continue

END SUBROUTINE read_feedback_params


!################################################################
!################################################################
!################################################################
!################################################################

END MODULE feedback_module
