'''
Read the hdf5 cooling table
Sam Geen, September 2014
'''

import h5py
import fortranfile
import numpy as np

fname = 'metalCoolingTable.hdf5'

def readhdf5():
    '''
    Read the data found in fname
    '''
    def readdset(name):
        # Reads a single dataset from the file
        dset = f[name]
        arr = np.zeros(dset.shape)
        dset.read_direct(arr)
        return arr
    # Open the file
    f = h5py.File(fname,"r")
    data = {}
    # Read each dataset
    for item in f.keys():
        data[item] = readdset(item)
    return data

def makefortran(noflux=False):
    '''
    Write a fortran table from the input file
    Uses the total cooling rate (i.e. cool - heat)
    '''
    print "Writing fortran table..."
    # Get the data as a dictionary
    data = readhdf5()
    # Set up table
    ndim = np.zeros((1),dtype='int32')+3 # 3 dimensions
    dims = np.zeros((4),dtype='int32')
    fluxes = data["FluxBins"]
    Ts = data["TemperatureBins"]
    Ds = data["DensityBins"]
    dummy = np.zeros((1),dtype='float64')
    if noflux:
        # No flux temperatures
        # These are the same as Ts, but with extra terms at high T
        # (At high T all gas is ionised so no point in doing fluxed versions)
        Ts = data["TemperatureBinsNoFlux"]
        fluxes = dummy
    dims[0] = len(fluxes)
    dims[1] = len(Ds)
    dims[2] = len(Ts)
    dims[3] = 1
    if noflux:
    	dims[0] = len(Ds)
    	dims[1] = len(Ts)
    	dims[2] = 1
    	dims[3] = 1
    if not noflux:
        table = 10.0**data["MetalCooling"] - \
            10.0**data["MetalHeating"]
    else:
        table = 10.0**data["MetalCoolingNoFlux"] - \
            10.0**data["MetalHeatingNoFlux"]
    print "TABLE MAXMIN", table.max(), table.min()
    # Open fortran file and write
    outname = "cooltable.dat"
    if noflux:
        outname = "cooltablenoflux.dat"
    file = fortranfile.FortranFile(outname,mode="wb")
    file.writeInts(ndim)
    file.writeInts(dims)
    if not noflux:
	file.writeReals(fluxes,prec='d')
    file.writeReals(Ds,prec='d')
    file.writeReals(Ts,prec='d')
    if noflux:
	file.writeReals(dummy,prec='d')
    file.writeReals(dummy,prec='d')
    file.writeReals(table,prec='d')
    file.close()
    print "Done"
    

if __name__=="__main__":
    makefortran()
    makefortran(noflux=True)
