module cloud_module
  use amr_parameters

  ! TODO - CLEAN THIS OUT

  !initial temperature used for the isothermal run
  real(dp)::temper
  real(dp)::temper_iso

  !feedback from jet
  logical:: jet = .false., rad_jet=.false. 
  real(dp)::Ucoef=1.
  real(dp):: mass_jet_sink=0. !mass above which a jets is included

  !Initial conditions parameter for the dense core
  real(dp)::bx_bound=0.
  real(dp)::by_bound=0.
  real(dp)::bz_bound=0.
  real(dp)::turb=0.
  real(dp)::dens0=0.
  real(dp)::V0=0.
  real(dp)::Height0=0.

  real(dp):: switch_solv=1.d20

  !Initial conditions parameter for the dense core
  real(dp)::mass_c=1.   !cloud mass in solar mass
  real(dp)::rap=1.      !axis ratio
  real(dp)::cont=1.     !density contras
  real(dp)::ff_sct=1.   !freefall time / sound crossing time
  real(dp)::ff_rt=1.    !freefall time / rotation time
  real(dp)::ff_act=1.   !freefall time / Alfven crossing time
  real(dp)::ff_vct=1.   !freefall time / Vrms crossing time
  real(dp)::thet_mag=0. !angle between magnetic field and rotation axis
  real(dp)::bl_fac=1.   !multiply calculated boxlen by this factor

end module cloud_module
