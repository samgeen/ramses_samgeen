!################################################################
! TAKEN FROM PATRICK HENNEBELLE'S sink_particle.f90 AND PUT HERE
! BY SAM GEEN, JANUARY 2015 
!################################################################
!################################################################
!################################################################
!################################################################
subroutine star_jet_feedback
  use amr_commons
  use pm_commons
  use hydro_commons
  use cooling_module, ONLY: XH=>X, rhoc, mH 
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
  integer::nSN_tot_all
  integer,dimension(1:ncpu)::nSN_icpu_all
  real(dp),dimension(:),allocatable::mSN_all,sSN_all,ZSN_all
  real(dp),dimension(:,:),allocatable::xSN_all,vSN_all
#endif
  !----------------------------------------------------------------------
  ! Description: This subroutine checks jet events in cells where a
  ! star particle has been spawned.
  ! Yohan Dubois
  !----------------------------------------------------------------------
  ! local constants
  integer::ip,icpu,igrid,jgrid,npart1,npart2,ipart,jpart,next_part
  integer::nSN,nSN_loc,nSN_tot,info,isink,ilevel,ivar
  integer,dimension(1:ncpu)::nSN_icpu
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,t0
  real(dp)::scale,dx_min,vol_min,nISM,nCOM,d0,mstar,temp_blast
  real(dp)::T2_AGN,T2_min,T2_max,delta_mass_max
  integer::nx_loc
  integer,dimension(:),allocatable::ind_part,ind_grid
  logical,dimension(:),allocatable::ok_free

  if(.not. hydro)return
  if(ndim.ne.3)return

!  if(verbose)write(*,*)'Entering star_jet_feedback'
  
  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Mesh spacing in that level
!  nx_loc=(icoarse_max-icoarse_min+1)
!  scale=boxlen/dble(nx_loc)
!  dx_min=(0.5D0**nlevelmax)*scale
!  vol_min=dx_min**ndim


  ! Compute the direction of the magnetic field in the sink
!  call compute_magnetic_field


  ! Compute the direction of the angular momentum around the sink
!  call compute_angular_momentum


  ! Compute the grid discretization effects
!  call average_jet

!#ifndef WITHOUTMPI
!  call MPI_ALLREDUCE(ok_blast_agn,ok_blast_agn_all,nsink,MPI_LOGICAL,MPI_LOR,MPI_COMM_WORLD,info)
!  ok_blast_agn=ok_blast_agn_all
!#endif

  ! Modify hydro quantities to account for the AGN blast
!  call put_jet


  ! Update hydro quantities for split cells
!  do ilevel=nlevelmax,levelmin,-1
!     call upload_fine(ilevel)
!     do ivar=1,nvar + 3
!        call make_virtual_fine_dp(uold(1,ivar),ilevel)
!     enddo
!  enddo

end subroutine star_jet_feedback
!################################################################
!################################################################
!################################################################
!################################################################
